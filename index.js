var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        var respuesta = JSON.parse(xhttp.responseText);
        console.log(respuesta);

        var personas = respuesta.personas;
        var imprimir = '';
        for (var i = 0; i < personas.length; i++) {
            imprimir +=
                `<div> 
                
                <strong>Cedula: </strong>${personas[i].cedula} <br>
                <strong>Nombre: </strong>  ${personas[i].nombre}<br>
                <strong>Direccion: </strong> ${personas[i].direccion}<br>
                <strong>Telefono: </strong> ${personas[i].telefono}<br>
                <strong>Correo: </strong> ${personas[i].correo}<br>
                <strong>Curso: </strong> ${personas[i].curso}<br>
                <strong>Paralelo: </strong> ${personas[i].paralelo}<br>

            
            </div>`
        }
        document.getElementById('personas').innerHTML = imprimir;
    }
};
xhttp.open("GET", "datos.json", true);
xhttp.send();
